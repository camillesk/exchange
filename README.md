# README
# Exchange APP
## Camille da Luz Bettini
## v1 - 16/06/19

This app runs with docker-compose

Application to learn Ruby on Rails, coffeescript, docker, continuous integration and rspec tests.

Features:

* Converts any currency using an API
