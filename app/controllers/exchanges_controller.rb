# frozen_string_literal: true

# Do the convertion and app index
class ExchangesController < ApplicationController
  def index; end

  def convert
    value = ExchangeService.new(params[:source_currency], params[:target_currency], params[:amount]).perform
    render json: { "value": value }
  end
end
